extends Area2D

onready var xbtn = $Sprite
onready var sprite = $AnimatedSprite

var canOpen: bool = false

func _ready():
	sprite.animation = "closed"

func _on_Chest_body_entered(body):
	if body is Player && GameController.hasKey:
		canOpen = true
		xbtn.visible = true

func _input(event):
	if event.is_action_pressed("X") && canOpen:
		
		GameController.player.canMove = false
		sprite.animation = "open"
		GameController.hud.end = true

func _on_Chest_body_exited(body):
	if body is Player:
		xbtn.visible = false
		canOpen = false
