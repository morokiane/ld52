extends Control

func _ready():
	VisualServer.set_default_clear_color(Color("9bbc0f"))
	$VBoxContainer/Start.grab_focus()

func _on_Start_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/MainLevel.scn")

func _on_HowTo_pressed():
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/HowToPlay.scn")

func _on_Exit_pressed():
	get_tree().quit()
