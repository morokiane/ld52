extends KinematicBody2D
class_name Player

enum {
	move,
	attack,
	passedout
}

onready var sprite = $AnimatedSprite
onready var remoteTransform = $RemoteTransform2D
onready var hurtbox = $Hurtbox/CollisionShape2D
onready var attackTimer = $AttackTimer
onready var flashTimer = $Timer
onready var collision = $CollisionShape2D
onready var cam = get_node("../Camera2D")

export var moveSpeed: float = 80
export var accel: float = 900
export var frict: float = 600
export var jheight: float = -85
export var jrelease: float = -70
export var fallSpeed: float = 3
export var grav: float = 200

var velocity: Vector2 = Vector2.ZERO
var lastPosition: Vector2 = Vector2.ZERO
var state = move
var doubleJump: int = 1
var canGetHit: bool = true
var canMove: bool = true

func _ready():
	GameController.player = self
	hurtbox.disabled = true
# warning-ignore:return_value_discarded
	GameController.connect("revive", self, "StartRevive")

func _physics_process(delta):
	var input = Vector2.ZERO
	input.x = Input.get_axis("left", "right")
	input.y = Input.get_axis("up", "down")
	
	if Input.is_action_just_pressed("down"):
		position.y += 2
	
	if Input.is_action_pressed("X"):
		state = attack
		
	if Input.is_action_just_pressed("A") && GameController.playerHP == 0:
		GameController.emit_signal("revive")
	
	if canMove:
		match state:
			move: Move(input, delta)
			attack: Attack(delta)
			passedout: PassedOut()

	if GameController.playerHP <= 0:
		state = passedout

func ApplyGravity(delta):
	velocity.y += grav * delta

func ApplyFriction(delta):
	velocity.x = move_toward(velocity.x, 0, frict * delta)
	
func ApplyAccel(amount, delta):
	velocity.x = move_toward(velocity.x, moveSpeed * amount, accel * delta)
	
func Move(input, delta):
	ApplyGravity(delta)
	
#	hurtbox.disabled = true
	if input.x == 0:
		ApplyFriction(delta)
		sprite.animation = "idle"
	else:
		ApplyAccel(input.x, delta)
		sprite.animation = "run"
		if input.x > 0:
			sprite.flip_h = false
			$Hurtbox.scale.x = 1
		else:
			sprite.flip_h = true
			$Hurtbox.scale.x = -1

	if is_on_floor():
		doubleJump = GameController.doubleJump
		
		if Input.is_action_just_pressed("A"):
			velocity.y = jheight
			SoundFx.play("jump")
	
	else:
		sprite.animation = "jump"
		if Input.is_action_just_released("A") && velocity.y < jrelease:
			velocity.y = jrelease

		if Input.is_action_just_pressed("A") && doubleJump > 0:
			velocity.y = jheight
			doubleJump = 0
		
		if velocity.y > 0:
			sprite.animation = "fall"
			velocity.y += fallSpeed

	velocity = move_and_slide(velocity, Vector2.UP)

func Attack(delta):
	ApplyGravity(delta)

	if velocity.y > 0:
		velocity.y += fallSpeed
	sprite.animation = "attack"
	
	if attackTimer.time_left == 0:
		hurtbox.disabled = false
		attackTimer.start()
	else:
		hurtbox.disabled = true
		
	if !is_on_floor():
		velocity = move_and_slide(velocity, Vector2.UP)
		
	state = move

func ConnectCamera(camera):
	var cameraPath = camera.get_path()
	remoteTransform.remote_path = cameraPath

func Flash():
	sprite.material.set_shader_param("flashModifier", 1)
	flashTimer.start()
	if !sprite.flip_h:
		velocity.x = -125
		velocity.y = -50
		sprite.animation = "hit"
	elif sprite.flip_h:
		velocity.x = 125
		velocity.y = -50
		sprite.animation = "hit"
	cam.shake(0.5,90,1)
	SoundFx.play("playerhit")

func _on_Timer_timeout():
	sprite.material.set_shader_param("flashModifier", 0)

func PassedOut():
	collision.disabled = true
	canMove = false
	sprite.animation = "dead"
	SoundFx.play("death")
	yield(get_tree().create_timer(0.5), "timeout")
	sprite.playing = false
	GameController.hud.showRevive = true
	
# warning-ignore:integer_division
	GameController.mushroomCollected = GameController.mushroomCollected / 2
# warning-ignore:integer_division
	GameController.fernCollected = GameController.fernCollected / 2
# warning-ignore:integer_division
	GameController.ironCollected = GameController.ironCollected / 2
# warning-ignore:integer_division
	GameController.slimeCollected = GameController.slimeCollected / 2
# warning-ignore:integer_division
	GameController.boneCollected = GameController.boneCollected / 2
# warning-ignore:integer_division
	GameController.rockCollected = GameController.rockCollected / 2
	
	GameController.hud.Reset()

func StartRevive():
	GameController.hud.showRevive = false
	sprite.playing = true
	sprite.animation = "revive"
	SoundFx.play("fairy")
	yield(get_tree().create_timer(0.8), "timeout")
	GameController.playerHP = 100
	sprite.animation = "idle"
	canMove = true
	collision.disabled = false
	state = move
