extends Node
var soundPath = "res://SFX/"

onready var soundPlayers = get_children()

var sounds = {
	"enemydie" : load(soundPath + "EnemyDie.wav"),
	"hurt" : load(soundPath + "Hurt.wav"),
	"jump" : load(soundPath + "Jump.wav"),
	"powerup" : load(soundPath + "Powerup.wav"),
	"land" : load(soundPath + "Land.wav"),
	"death" : load(soundPath + "Death.wav"),
	"hit" : load(soundPath + "Hit.wav"),
	"planthit" : load(soundPath + "PlantHit.wav"),
	"rockbroke" : load(soundPath + "RockBroke.wav"),
	"hardmetal" : load(soundPath + "HardMetal.wav"),
	"playerhit" : load(soundPath + "PlayerHit.wav"),
	"fairy" : load(soundPath + "Fairy.wav"),
	"pit" : load(soundPath + "Pit.wav"),
	"gameboystartup" : load(soundPath + "GameboyStartUp.mp3")
}

func play(soundString):
	for soundPlayer in soundPlayers:
		if not soundPlayer.playing:
			soundPlayer.stream = sounds[soundString]
			soundPlayer.play()
			return
