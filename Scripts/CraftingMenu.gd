extends CanvasLayer

onready var info = $Info
#onready var bone = $HBoxContainer/Bone
#onready var fern = $HBoxContainer/Fern
#onready var iron = $HBoxContainer/Iron
#onready var mushroom = $HBoxContainer/MushroomNum
#onready var rock = $HBoxContainer/RockNum
#onready var slime = $HBoxContainer/SlimeNum
onready var boneNum = $HBoxContainer/Bone/BoneNum
onready var fernNum = $HBoxContainer/Fern/FernNum
onready var ironNum = $HBoxContainer/Iron/IronNum
onready var mushroomNum = $HBoxContainer/Mushroom/MushroomNum
onready var rockNum = $HBoxContainer/Rock/RockNum
onready var slimeNum = $HBoxContainer/Slime/SlimeNum

onready var harvestedBoneNum = $HBoxContainer2/Bone/BoneNum
onready var harvestedFernNum = $HBoxContainer2/Fern/FernNum
onready var harvestedIronNum = $HBoxContainer2/Iron/IronNum
onready var harvestedMushroomNum = $HBoxContainer2/Mushroom/MushroomNum
onready var harvestedRockNum = $HBoxContainer2/Rock/RockNum
onready var harvestedSlimeNum = $HBoxContainer2/Slime/SlimeNum

onready var flubber = $VBoxContainer/Flubber
onready var boneFork = $VBoxContainer/BoneFork
onready var rockFork = $VBoxContainer/RockFork
onready var ironFork = $VBoxContainer/IronFork
onready var ironKey = $VBoxContainer/IronKey

func _ready():
	VisualServer.set_default_clear_color(Color("9bbc0f"))
	$VBoxContainer/HPPot.grab_focus()
	
	if GameController.doubleJump == 0:
		flubber.visible = true
	else:
		flubber.visible = false
		
	if GameController.forkDamage >= 2:
		boneFork.visible = false
	else:
		boneFork.visible = true
		
	if GameController.forkDamage >= 3:
		rockFork.visible = false
	else:
		rockFork.visible = true

	if GameController.forkDamage >= 4:
		ironFork.visible = false
	else:
		ironFork.visible = true
		
	if GameController.hasKey:
		ironKey.visible = false
	else:
		ironKey.visible = true

func _process(_delta):
	harvestedBoneNum.text = String(GameController.boneCollected)
	harvestedFernNum.text = String(GameController.fernCollected)
	harvestedIronNum.text = String(GameController.ironCollected)
	harvestedMushroomNum.text = String(GameController.mushroomCollected)
	harvestedRockNum.text = String(GameController.rockCollected)
	harvestedSlimeNum.text = String(GameController.slimeCollected)

func _input(event):
	if event.is_action_pressed("B"):
		if GameController.ironCooldown > 0:
			GameController.ironCooldown -= 1
		if GameController.rockCooldown > 0:
			GameController.rockCooldown -= 1
		GameController.day += 1
		#terrible terrible coding here
		if GameController.day > 3 && GameController.day < 6:
			if GameController.boneCollected > 0:
				GameController.boneCollected -= 1
			if GameController.fernCollected > 0:
				GameController.fernCollected -= 1
			if GameController.mushroomCollected > 0:
				GameController.mushroomCollected -= 1
			if GameController.slimeCollected > 0:
				GameController.slimeCollected -= 1
		elif GameController.day > 6 && GameController.day < 12:
			if GameController.boneCollected > 0 && GameController.boneCollected > 2:
				GameController.boneCollected -= 2
			if GameController.fernCollected > 0 && GameController.fernCollected > 2:
				GameController.fernCollected -= 2
			if GameController.mushroomCollected > 0 && GameController.mushroomCollected > 2:
				GameController.mushroomCollected -= 2
			if GameController.slimeCollected > 0 && GameController.slimeCollected > 2:
				GameController.slimeCollected -= 2
		elif GameController.day > 12 && GameController.day < 18:
			if GameController.boneCollected > 0 && GameController.boneCollected > 3:
				GameController.boneCollected -= 3
			if GameController.fernCollected > 0 && GameController.fernCollected > 3:
				GameController.fernCollected -= 3
			if GameController.ironCollected > 0 && GameController.ironCollected > 3:
				GameController.ironCollected -= 3
			if GameController.mushroomCollected > 0 && GameController.mushroomCollected > 3:
				GameController.mushroomCollected -= 3
			if GameController.rockCollected > 0 && GameController.rockCollected > 3:
				GameController.rockCollected -= 3
			if GameController.slimeCollected > 0 && GameController.slimeCollected > 3:
				GameController.slimeCollected -= 3
		elif GameController.day > 18:
			if GameController.boneCollected > 0 && GameController.boneCollected > 4:
				GameController.boneCollected -= 4
			if GameController.fernCollected > 0 && GameController.fernCollected > 4:
				GameController.fernCollected -= 4
			if GameController.mushroomCollected > 0 && GameController.mushroomCollected > 4:
				GameController.mushroomCollected -= 4
			if GameController.slimeCollected > 0 && GameController.slimeCollected > 4:
				GameController.slimeCollected -= 4
		
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Scenes/MainLevel.scn")

func _on_HPPot_focus_entered():
	info.text = "Restore all of your health. Bottoms up."
	boneNum.text = String("0")
	fernNum.text = String("0")
	ironNum.text = String("0")
	mushroomNum.text = String("8")
	rockNum.text = String("0")
	slimeNum.text = String("5")

func _on_Flubber_focus_entered():
	info.text = "Allows you to double jump. So bouncy!"
	boneNum.text = String("0")
	fernNum.text = String("20")
	ironNum.text = String("0")
	mushroomNum.text = String("5")
	rockNum.text = String("0")
	slimeNum.text = String("10")

func _on_BoneFork_focus_entered():
	info.text = "Upgrade fork to take on harder materials. No rock will stand in your way. More items will be harvestable."
	boneNum.text = String("15")
	fernNum.text = String("20")
	ironNum.text = String("0")
	mushroomNum.text = String("0")
	rockNum.text = String("0")
	slimeNum.text = String("10")

func _on_RockFork_focus_entered():
	info.text = "Upgrade fork to harvest iron. More items will be harvestable. Fire up the forges!"
	boneNum.text = String("25")
	fernNum.text = String("20")
	ironNum.text = String("0")
	mushroomNum.text = String("0")
	rockNum.text = String("15")
	slimeNum.text = String("10")

func _on_IronFork_focus_entered():
	info.text = "Seriously no rock will stand in your way. Especially those crumbly ones. More items will be harvestable."
	boneNum.text = String("60")
	fernNum.text = String("10")
	ironNum.text = String("15")
	mushroomNum.text = String("60")
	rockNum.text = String("10")
	slimeNum.text = String("20")

func _on_IronKey_focus_entered():
	info.text = "Opens the super secret chest in the forest."
	boneNum.text = String("75")
	fernNum.text = String("75")
	ironNum.text = String("30")
	mushroomNum.text = String("30")
	rockNum.text = String("0")
	slimeNum.text = String("45")

func _on_HPPot_pressed():
	if GameController.mushroomCollected >= 8 && GameController.slimeCollected >= 5:
		GameController.mushroomCollected -= 8
		GameController.slimeCollected -= 5
		GameController.playerHP = 100

func _on_Flubber_pressed():
	if GameController.fernCollected >= 20 && GameController.mushroomCollected>= 5 && GameController.slimeCollected >= 10:
		GameController.fernCollected -= 20
		GameController.mushroomCollected -= 5
		GameController.slimeCollected -= 10
		GameController.doubleJump = 1
		flubber.visible = false
		$VBoxContainer/HPPot.grab_focus()

func _on_BoneFork_pressed():
	if GameController.boneCollected >= 15 && GameController.fernCollected >= 20 && GameController.slimeCollected >= 10:
		GameController.boneCollected -= 15
		GameController.fernCollected -= 20
		GameController.slimeCollected -= 10
		GameController.forkDamage = 2
		GameController.numOfDrops = 3
		boneFork.visible = false
		$VBoxContainer/HPPot.grab_focus()

func _on_RockFork_pressed():
	if GameController.boneCollected >= 25 && GameController.fernCollected >= 20 && GameController.rockCollected >= 15 && GameController.slimeCollected >= 10:
		GameController.boneCollected -= 25
		GameController.fernCollected -= 20
		GameController.rockCollected -= 15
		GameController.slimeCollected -= 10
		GameController.forkDamage = 3
		GameController.numOfDrops = 4
		rockFork.visible = false
		$VBoxContainer/HPPot.grab_focus()
		
func _on_IronFork_pressed():
	if GameController.boneCollected >= 60 && GameController.fernCollected >= 10 && GameController.ironCollected >= 15 && GameController.mushroomCollected >= 60 && GameController.rockCollected >= 10 && GameController.slimeCollected >= 20:
		GameController.boneCollected -= 60
		GameController.fernCollected -= 10
		GameController.ironCollected -= 15
		GameController.mushroomCollected -= 60
		GameController.rockCollected -= 10
		GameController.slimeCollected -= 20
		GameController.forkDamage = 4
		GameController.numOfDrops = 5
		ironFork.visible = false
		$VBoxContainer/HPPot.grab_focus()

func _on_IronKey_pressed():
	if GameController.boneCollected >= 75 && GameController.fernCollected >= 75 && GameController.ironCollected >= 30 && GameController.mushroomCollected >= 30 && GameController.slimeCollected >= 45:
		GameController.boneCollected -= 75
		GameController.fernCollected -= 75
		GameController.ironCollected -= 30
		GameController.mushroomCollected -= 30
		GameController.slimeCollected -= 45
		GameController.hasKey = true
		ironKey.visible = false
		$VBoxContainer/HPPot.grab_focus()
