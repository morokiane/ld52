extends Area2D

export var botRightY: Vector2 = Vector2.ZERO

func _on_MoveCam_body_entered(body):
	if body is Player:
		GameController.cam.botRight.global_position.y = botRightY.y

