extends Node2D

onready var clouds = $Sprite

var flip = 0

func _ready():
	randomize()
	clouds.frame = randi() % 4
	flip = randi() % 2
	
	if flip == 0:
		clouds.flip_h = false
	else:
		clouds.flip_h = true
