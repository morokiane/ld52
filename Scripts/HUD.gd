extends CanvasLayer

onready var progressBar := $TextureProgress
onready var bone := $VBoxContainer/Bone
onready var fern := $VBoxContainer/Fern
onready var iron := $VBoxContainer/Iron
onready var mushroom := $VBoxContainer/MushroomNum
onready var rock := $VBoxContainer/RockNum
onready var slime := $VBoxContainer/SlimeNum
onready var boneNum := $VBoxContainer/Bone/BoneNum
onready var fernNum := $VBoxContainer/Fern/FernNum
onready var ironNum := $VBoxContainer/Iron/IronNum
onready var mushroomNum := $VBoxContainer/MushroomNum/MushroomNum
onready var rockNum := $VBoxContainer/RockNum/RockNum
onready var slimeNum := $VBoxContainer/SlimeNum/SlimeNum
onready var passedOut := $PassedOut
onready var daynum := $DayNum
onready var key = $Key
onready var fin := $Control
onready var finJours := $Control/FinJours

var showRevive: bool = false
var end: bool = false

func _ready():
	GameController.hud = self
	daynum.text = String(GameController.day)

	bone.visible = false
	fern.visible = false
	iron.visible = false
	mushroom.visible = false
	rock.visible = false
	slime.visible = false
	fin.visible = false

	progressBar.value = GameController.playerHP
	
func _process(_delta):
	if showRevive:
		passedOut.visible = true
	else:
		passedOut.visible = false
	
	progressBar.value = GameController.playerHP
	
	if GameController.boneCollected > 0:
		bone.visible = true
		boneNum.text = String(GameController.boneCollected)
	if GameController.fernCollected > 0:
		fern.visible = true
		fernNum.text = String(GameController.fernCollected)
	if GameController.ironCollected > 0:
		iron.visible = true
		ironNum.text = String(GameController.ironCollected)
	if GameController.mushroomCollected > 0:
		mushroom.visible = true
		mushroomNum.text = String(GameController.mushroomCollected)
	if GameController.rockCollected > 0:
		rock.visible = true
		rockNum.text = String(GameController.rockCollected)
	if GameController.slimeCollected > 0:
		slime.visible = true
		slimeNum.text = String(GameController.slimeCollected)
	
	if GameController.hasKey:
		key.visible = true
		
	if end:
		fin.visible = true
		finJours.text = "Finished in " + str(GameController.day) + " days"
		
func _input(event):
	if event.is_action_pressed("B") && end:
		get_tree().quit()

func Reset():
	if GameController.boneCollected <= 0:
		bone.visible = false
		boneNum.text = String(GameController.boneCollected)
	if GameController.fernCollected <= 0:
		fern.visible = false
		fernNum.text = String(GameController.fernCollected)
	if GameController.ironCollected <= 0:
		iron.visible = false
		ironNum.text = String(GameController.ironCollected)
	if GameController.mushroomCollected <= 0:
		mushroom.visible = false
		mushroomNum.text = String(GameController.mushroomCollected)
	if GameController.rockCollected <= 0:
		rock.visible = false
		rockNum.text = String(GameController.rockCollected)
	if GameController.slimeCollected <= 0:
		slime.visible = false
		slimeNum.text = String(GameController.slimeCollected)

func Refresh():
	boneNum.text = String(GameController.boneCollected)
	fernNum.text = String(GameController.fernCollected)
	ironNum.text = String(GameController.ironCollected)
	mushroomNum.text = String(GameController.mushroomCollected)
	rockNum.text = String(GameController.rockCollected)
	slimeNum.text = String(GameController.slimeCollected)
