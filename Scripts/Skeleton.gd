extends KinematicBody2D

const bone = preload("res://Scenes/BoneCollect.scn")

export var hp: int = 4
export var playerDamage: int = 0
export var avoidFalling: bool = false

onready var sprite:= $AnimatedSprite
onready var ledgeCheck:= $LedgeCast
onready var flashTimer:= $Timer
onready var visibility:= $VisibilityNotifier2D

var direction: Vector2 = Vector2.LEFT
var velocity: Vector2 = Vector2.ZERO
var gravity: float = 300
var lastPosition: Vector2 = Vector2.ZERO
var numDrops: int = 0
var posX: Vector2 = Vector2.ZERO

func _ready():
# warning-ignore:return_value_discarded
	visibility.connect("screen_entered", self, "show")
# warning-ignore:return_value_discarded
	visibility.connect("screen_exited", self, "hide")
	visible = false
	randomize()
	numDrops = randi() % GameController.numOfDrops + 1
	
func _physics_process(delta):
	Move(delta)
	
	if hp <= 0:
		Kill()
		
func Move(delta):
	velocity.y += gravity * delta
	
	var foundWall = is_on_wall()
	var foundLedge = not ledgeCheck.is_colliding()
	
	if foundWall:
		direction *= -1
	
	if foundLedge:
		direction *= -1
	
	sprite.flip_h = direction.x > 0
	
	if is_on_floor():
		velocity = direction * -10

# warning-ignore:return_value_discarded
	velocity = move_and_slide(velocity, Vector2.UP)
	
func Kill():
	lastPosition = self.position
	
	while numDrops > 0:
		var loot: RigidBody2D = bone.instance()
		loot.set_position(lastPosition)
		get_parent().call_deferred("add_child", loot)
		numDrops -= 1
	SoundFx.play("enemydie")
	queue_free()

func _on_Hitbox_body_entered(body):
	if body is Player:
#		posX = get_parent().position.x
		GameController.playerHP -= playerDamage
		SoundFx.play("hit")
		GameController.player.Flash()

func _on_Hurtbox_area_entered(area):
	if area.is_in_group("fork"):
		hp -= GameController.forkDamage
		SoundFx.play("hit")
		Flash()

func Flash():
	sprite.material.set_shader_param("flashModifier", 1)
	flashTimer.start()

func _on_Timer_timeout():
	sprite.material.set_shader_param("flashModifier", 0)
