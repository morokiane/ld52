extends Node

# warning-ignore:unused_signal
signal revive

var shop = {}

var player
var hud
var cam

var playerHP: int = 100
var doubleJump: int = 0
var forkDamage: int = 1
var hasKey: bool = false
var day: int = 1

var numOfDrops = 2

var mushroomCollected: int = 0
var fernCollected: int = 0
var ironCollected: int = 0
var slimeCollected: int = 0
var boneCollected: int = 0
var rockCollected: int = 0

var ironCooldown = 0
var rockCooldown = 0

func _input(event):
	if event.is_action_pressed("close"):
		get_tree().quit()

	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

func _ready():
	ImportData()
	
var my_dict = {"Item": ["Bone", "Fern", "Iron", "Mush", "Rock", "Slime"],
			   "HP Pot": [0, 0, 0, 8, 0, 5],
			   "Flubber": [0, 20, 0, 5, 0, 10],
			   "Bone Fork": [15, 20, 0, 0, 0, 10],
			   "Rock Fork": [25, 20, 0, 0, 15, 10],
			   "Iron Fork": [60, 10, 15, 60, 10, 20],
			   "Iron Key": [75, 75, 30, 30, 0, 45]}

func ImportData():
	var file = File.new()
	file.open("res://Data.txt", file.READ)

	while !file.eof_reached():
		var data_set = Array(file.get_csv_line())
		shop[shop.size()] = data_set
	
	file.close()
	print(shop)
	print(shop.get(int("2"))[2])
