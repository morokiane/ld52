 extends Control

onready var logo = $Logo
onready var sfx = $AudioStreamPlayer

func _ready():
	VisualServer.set_default_clear_color(Color("9bbc0f"))
	var tween = create_tween().set_trans(Tween.TRANS_LINEAR)
	tween.tween_property(logo, "rect_position", Vector2(54, 43), 3)

func _on_Timer_timeout():
	sfx.play()
	yield(get_tree().create_timer(2.0), "timeout")
	sfx.stop()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/TitleScreen.scn")
