extends RigidBody2D

func _ready():
	self.linear_velocity.x = rand_range(-25, 25)
	self.linear_velocity.y = rand_range(-75, -100)

func _on_Area2D_body_entered(body):
	if body is Player:
		GameController.mushroomCollected += 1
		SoundFx.play("powerup")
		queue_free()
