extends Node2D

func _on_Area2D_body_entered(body):
	if body is Player:
		GameController.player.global_position = GameController.player.lastPosition
		GameController.playerHP -= 25
		SoundFx.play("pit")
