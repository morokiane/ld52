extends Node2D

onready var xbtn = $XBtn

var openShop: bool = false

func _on_Area2D_body_entered(body):
	if body is Player:
		xbtn.visible = true
		openShop = true

func _on_Area2D_body_exited(body):
	if body is Player:
		xbtn.visible = false
		openShop = false
	
func _input(event):
	if event.is_action_pressed("X") && openShop:
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Scenes/CraftingMenu.tscn")
