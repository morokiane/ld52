extends StaticBody2D

const drop = preload("res://Scenes/RockCollect.scn")

export var flip: bool = false
export var stoneBlock: bool = false

onready var flashTimer = $Timer
onready var sprite = $Sprite
onready var visibility:= $VisibilityNotifier2D

var lastPosition: Vector2 = Vector2.ZERO
var hp: int = 10
var numDrops: int = 0

func _ready():
# warning-ignore:return_value_discarded
	visibility.connect("screen_entered", self, "show")
# warning-ignore:return_value_discarded
	visibility.connect("screen_exited", self, "hide")
	visible = false
	randomize()
	numDrops = randi() % GameController.numOfDrops + 1
	
	if flip:
		sprite.flip_h = true
	
func _process(_delta):
	if hp <= 0:
		Kill()

func Kill():
	lastPosition = self.position
	while numDrops > 0:
		var loot: RigidBody2D = drop.instance()
		loot.set_position(lastPosition)
		get_parent().call_deferred("add_child", loot)
		numDrops -= 1
	SoundFx.play("rockbroke")
	queue_free()

func Flash():
	sprite.material.set_shader_param("flashModifier", 1)
	flashTimer.start()

func _on_Timer_timeout():
	sprite.material.set_shader_param("flashModifier", 0)

func _on_Area2D_area_entered(area):
	if area.is_in_group("fork") && GameController.forkDamage >= 4 && stoneBlock:
		Flash()
		SoundFx.play("hit")
		hp -= GameController.forkDamage
	elif area.is_in_group("fork") && GameController.forkDamage >= 2 && !stoneBlock:
		Flash()
		SoundFx.play("hit")
		hp -= GameController.forkDamage
	else:
		SoundFx.play("hardmetal")
