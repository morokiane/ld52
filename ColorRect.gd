extends ColorRect

var pixel_size = Vector2(.01, .01)
var dot_color = Color(1, 0, 0, 1)

func _ready():
	var file = File.new()
	file.open("res://DotMatrixShader.shader", File.READ)
	var shader_code = file.get_as_text()
	file.close()
	var shader = Shader.new()
	shader.code = shader_code
	var material = ShaderMaterial.new()
	material.shader = shader
	material.set_shader_param("u_pixel_size", pixel_size)
	material.set_shader_param("u_dot_color", dot_color)
	self.material = material
