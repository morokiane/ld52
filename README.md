# LD52

Welcome to Forked Up. A game about forking up, I mean harvesting items to upgrade your fork and create the key to the super secret chest. 

Gather plants, kill things and harvest their resources to take back to the shop to craft better stuff. Be careful though, if you die in the field you will lose half of what you've harvested.

Weight the risk of keep going or go back to the shop for a health potion. Each time you leave the shop is a new day, and new consquences to loitering.

Controller is highly recommended. 
Keyboard controls:
Esc - Quit game
Alt-Enter - Go to game window
Arrow key - move; Ctrl - Attack; Space - Jump; Enter - Confirm shop item
You can also move with WASD and use Mouse button 1 to attack.